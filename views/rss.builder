xml.instruct! :xml, :version => '1.0'
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "Last books"
    xml.description "Books from Packt."
    xml.link "http://37.17.173.96/rss"

    @books.each do |book|
      xml.item do
        xml.title book[:title].force_encoding("UTF-8")
        xml.link book[:claim_url]
        xml.pubDate Time.at(book[:grabbed_at]).rfc822()
        xml.guid book[:claim_url]
      end
    end
  end
end
