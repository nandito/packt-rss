require "sinatra"
require "builder"

# TODO set up environments
# TODO grab input file from ENV
# TODO ne fossa össze magát, ha nem jó az input
# - blokkosított olvasás:
#   - blokk start: "----------- Packt Grab Started --------------"
#   - blokk end: "----------- Packt Grab Done --------------"
# - ha a blokkban van "Book Title:" - grab it!
# - ha a blokkban van "Claim url:" - grab it!
# - ha a blokk csak "Packt Grab Started" és "Pack Grab Done"-ból áll - input error
# TODO tesztek
# TODO elemek számának korlátozása

def read_books
  books = []
  lines = File.readlines("data/cron_output")
  lines.map! { |e| e.chomp }

  lines.each_with_index do |item,index|
    if (index+1)%5 == 0
      books.push({
        "title": lines[index-3][12..-1],
        "claim_url": lines[index-2][11..-1],
	      "grabbed_at": Integer(lines[index-1][12..-1])
      })
    end
  end
  books.reverse
end

get '/rss' do
  @books = read_books
  builder :rss
end
