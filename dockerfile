FROM ruby:2.2.3

RUN gem install sinatra
RUN gem install builder

# Set environment variable
ENV HOME /home/packt-rss

WORKDIR $HOME

# Add the app code
ADD . $HOME

# Healthcheck
HEALTHCHECK CMD curl --fail http://localhost:8080/rss || exit 1

# Start server on 8080 port
CMD ["ruby", "server.rb", "-o", "0.0.0.0", "-p", "8080"]
